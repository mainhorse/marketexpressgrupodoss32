CREATE DATABASE marketExpress CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE marketExpress;

CREATE TABLE usuario(
    idUsuario INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tipo INT,
    nombre VARCHAR(255),
    apellido VARCHAR(255),
    correo VARCHAR(255),
    contrasena VARCHAR(255) ,
    avatar VARCHAR(255),
    telefono VARCHAR(255),
    direccion VARCHAR(255),
    compras  VARCHAR(255),
    carrito VARCHAR(255)
);

CREATE TABLE roles(
    idRole INT NOT NULL AUTO_INCREMENT PRIMARY kEY,
    nombreRole Varchar(255)
);

INSERT INTO roles (nombreRole) VALUES('SuperAdministrador'),('Administrador'),('Usuario');
ALTER TABLE usuario ADD CONSTRAINT fk_usuario FOREIGN KEY (tipo) REFERENCES roles (idRole);

CREATE TABLE unidad (
    idUnidad INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    unidad VARCHAR(255)
);

INSERT INTO unidad (unidad) VALUES ('Kilo'),('Libra'),('Unidad'),('Docena'),('Cubeta'),('Caja'),('Botella'),('Arroba'),('Bulto'),('Bolsa');

CREATE TABLE verduras(
    idVerduras INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subverduras (
    idSubVerduras INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubVerduras Varchar(255)
);

INSERT INTO subverduras (nombreSubVerduras) VALUE ('Frutas'),('vegetales'),('granos'),('especies');
ALTER TABLE verduras ADD CONSTRAINT fk_subVerduras FOREIGN KEY (categoria) REFERENCES subverduras (idSubVerduras);
ALTER TABLE verduras ADD CONSTRAINT fk_unidadVerduras FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE hogar(
    idHogar INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT(50)
);

CREATE TABLE subHogar(
    idSubHogar INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubHogar VARCHAR(255)
);

INSERT INTO subHogar (nombreSubHogar) VALUES  ('Cocina'), ('Decoración') ,('Muebles'),('Dormitorio');
ALTER TABLE hogar ADD CONSTRAINT fk_hogar FOREIGN KEY (categoria) REFERENCES subHogar (idSubHogar);
ALTER TABLE hogar ADD CONSTRAINT fk_unidadHogar FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE viveres(
    idViveres INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subViveres(
    idSubviveres INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubViveres VARCHAR(255)
);

INSERT INTO subViveres (nombreSubViveres) VALUES ('Enlatados'),('Pastas'),('Cereales'),('Aceites');
ALTER TABLE viveres ADD CONSTRAINT fk_viveres FOREIGN KEY (categoria) REFERENCES subViveres (idSubViveres);
ALTER TABLE viveres ADD CONSTRAINT fk_unidadViveres FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE electronicos(
    idElectronicos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT(50)
);

CREATE TABLE subElectronicos (
    idSubElectronico INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubElectronicos VARCHAR(255)
);

INSERT INTO subElectronicos (nombreSubElectronicos) VALUES ('celulares'),('computadores'),('tablets'),('Televisores');
ALTER TABLE electronicos ADD CONSTRAINT fk_electronicos FOREIGN KEY (categoria) REFERENCES subElectronicos (idSubElectronico);
ALTER TABLE electronicos ADD CONSTRAINT fk_unidadElectronicos FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE electrodomesticos (
    idElectrodomesticos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subElectrodomesticos(
    idSubElectrodomesticos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubElectrodomesticos VARCHAR(255)
);

INSERT INTO subElectrodomesticos (nombreSubElectrodomesticos) VALUES ('Neveras'),('Estufas'),('Horno'),('Lavadora');
ALTER TABLE electrodomesticos ADD CONSTRAINT fk_electrodomesticos FOREIGN KEY (categoria) REFERENCES subElectrodomesticos (idSubElectrodomesticos);
ALTER TABLE electrodomesticos ADD CONSTRAINT fk_unidadElectrodomesticos FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE videoJuegos (
    idVideoJuegos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subVideoJuegos(
    idsubVideoJuegos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubVideoJuegos VARCHAR(255)
);

INSERT INTO subVideoJuegos (nombreSubVideoJuegos) VALUE ('Accion'),('Arcade'),('Deportivo'),('Estrategia');
ALTER TABLE videoJuegos ADD CONSTRAINT fk_videojuegos FOREIGN KEY (categoria) REFERENCES subVideoJuegos (idSubVideoJuegos);
ALTER TABLE videoJuegos ADD CONSTRAINT fk_unidadVideoJuegos FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE licores (
    idLicores INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subLicores (
    idSubLicores INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubLicores VARCHAR(255)
);

INSERT INTO subLicores (nombreSubLicores) VALUE ('Vodka'),('Whisky'),('Ron'),('Aguardiente');
ALTER TABLE licores ADD CONSTRAINT fk_licor FOREIGN KEY (categoria) REFERENCES subLicores (idSubLicores);
ALTER TABLE licores ADD CONSTRAINT fk_unidadLicores FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE cervezas (
    idCervezas INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subCervezas (
    idSubCervezas INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubCervezas VARCHAR(255)
);

INSERT INTO subCervezas (nombreSubCervezas) VALUES ('Heineken'),('Corona'),('Budweiser'),('Aguila');
ALTER TABLE cervezas ADD CONSTRAINT fk_cervezas FOREIGN KEY (categoria) REFERENCES subCervezas (idSubCervezas);
ALTER TABLE cervezas ADD CONSTRAINT fk_unidadCerveza FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE vinos (
    idVinos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subVinos(
    idsubVinos INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubVinos VARCHAR(255)
);

INSERT INTO subVinos (nombreSubVinos) VALUES ('Blanco'), ('Rosado'), ('Tinto'), ('Clarete');
ALTER TABLE vinos ADD CONSTRAINT fk_subVinos FOREIGN KEY (categoria) REFERENCES subVinos (idsubVinos);
ALTER TABLE vinos ADD CONSTRAINT fk_unidadVinos FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE carnes (
    idCarnes INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subCarnes(
    idSubCarnes INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreSubCarnes VARCHAR(255)
);

INSERT INTO SubCarnes (nombreSubCarnes) VALUE ('Cerdo'), ('pescado'),('Pollo'),('Res');
ALTER TABLE carnes ADD CONSTRAINT fk_subCarnes FOREIGN KEY (categoria) REFERENCES subCarnes (idSubCarnes);
ALTER TABLE carnes ADD CONSTRAINT fk_unidadCarnes FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE aseoPersonal(
    idAseoPersonal INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subAseoPersonal(
    idSubAseoPersonal INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreAseoPersonal VARCHAR(255)
);

INSERT INTO subAseoPersonal (nombreAseoPersonal) VALUES ('jabon'), ('Bucal'),('Desodorante'), ('otros');
ALTER TABLE aseoPersonal ADD CONSTRAINT fk_subAseoPersonal FOREIGN KEY (categoria) REFERENCES subAseoPersonal (idSubAseoPersonal);
ALTER TABLE aseoPersonal ADD CONSTRAINT fk_unidadAseoPersonal FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);

CREATE TABLE aseoHogar (
    idAseoHogar INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    nombre VARCHAR(255),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valorDescuento INT
);

CREATE TABLE subAseoHogar(
    idSubAseoHogar INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombreAseoHogar VARCHAR(255)
);

INSERT INTO subAseoHogar(nombreAseoHogar) VALUES ('Detergentes'),('Suavizante'),('Ambientadores'),('Limpiadores');
ALTER TABLE aseoHogar ADD CONSTRAINT fk_subAseoHogar FOREIGN KEY (categoria) REFERENCES subAseoHogar (idSubAseoHogar);
ALTER TABLE aseoHogar ADD CONSTRAINT fk_unidadAseoHogar FOREIGN KEY (unidad) REFERENCES unidad (idUnidad);
