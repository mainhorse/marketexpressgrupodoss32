package com.api.marketExpress.repository;

import com.api.marketExpress.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    Product findById(int id);
    void delete(Product product);
    List<Product> findAll();
    List<Product> findByCategoriaContaining(String nombre);
    List<Product> findByNombreContains(String nombre);
    List<Product> findByCategoriaIsContaining(String nombre);
    List<Product> findByNombreLike(String nombre);

}


