package com.api.marketExpress.repository;

import com.api.marketExpress.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    User findById(int id);
    void delete(User user);
}
