package com.api.marketExpress.controller;


import com.api.marketExpress.model.Product;
import com.api.marketExpress.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)

    @PostMapping("/add")
    public Product add (@RequestBody Product product){
        productService.saveProduct(product);
        return product;
    }

    @PutMapping("/{id}")
    public Product updateProduct(@RequestBody Product product, @PathVariable("id") int id){
        product.setId(id);
        return productService.updateProduct(product);
    }

    @GetMapping("")
    public List<Product> findAll(){
        return productService.findAll();
    }

    @GetMapping("/search")
    public List<Product> findByName(String nombre){
        List<Product> product = productService.findByNombreLike(nombre);
        return product;
    }

    @DeleteMapping("/{id}")
    public Product deleteProduct(int id){
        return productService.deleteProduct(id);
    }
}
