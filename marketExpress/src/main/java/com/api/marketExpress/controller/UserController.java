package com.api.marketExpress.controller;

import com.api.marketExpress.model.User;
import com.api.marketExpress.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)

    @PostMapping("/add")
    public User add (@RequestBody User user){
        userService.saveUser(user);
        return user;
    }

    @GetMapping("/{id}")
    public User SearchUser (@PathVariable("id") int id){
        return userService.findUserId(id);
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestBody User user, @PathVariable("id") int id){
        user.setId(id);
        return userService.updateUser(user);
    }

    @DeleteMapping("/{id}")
    public User deleteUser (@PathVariable("id") int id){
        return  userService.deleteUser(id);
    }

}
