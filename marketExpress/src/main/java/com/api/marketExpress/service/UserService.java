package com.api.marketExpress.service;

import com.api.marketExpress.model.User;

public interface UserService {
    public User saveUser(User user);
    public User findUserId(int id);
    public User deleteUser(int id);
    public User updateUser(User user);
}
