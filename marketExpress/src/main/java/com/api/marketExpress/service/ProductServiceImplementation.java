package com.api.marketExpress.service;


import com.api.marketExpress.model.Product;
import com.api.marketExpress.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImplementation implements  ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    @Query("SELECT * FROM carnes WHERE nombre LIKE CONCAT ('%',:nombre, '%')")
    public List<Product> findByNombreLike(@Param("nombre") String nombre){
        return productRepository.findByNombreContains(nombre);
    }

    @Override
    public List<Product> findAll(){
        return productRepository.findAll();
    }

    @Override
    public Product findProductId(int id){

        return productRepository.findById(id);
    }

    @Override
    public Product updateProduct(Product product){
        return productRepository.save(product);
    }


    @Override
    public Product deleteProduct(int id){
        Product product = productRepository.findById(id);
        if(product != null){
            productRepository.delete(product);
        }
        return product;
    }

}
