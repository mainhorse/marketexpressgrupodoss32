package com.api.marketExpress.service;

import com.api.marketExpress.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Product saveProduct(Product product);
    Product findProductId(int id);
    Product deleteProduct(int id);
    Product updateProduct(Product product);
    List<Product> findAll();
    List<Product> findByNombreLike(String nombre);
}
