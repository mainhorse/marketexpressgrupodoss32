package com.api.marketExpress.service;


import com.api.marketExpress.model.User;
import com.api.marketExpress.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImplementation implements  UserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findUserId (int id){
        return userRepository.findById(id);
    }

    @Override
    public User updateUser(User user){
        return userRepository.save(user);
    }


    @Override
    public User deleteUser(int id){
        User user = userRepository.findById(id);
        if(user != null){
            userRepository.delete(user);
        }
        return user;
    }




}
