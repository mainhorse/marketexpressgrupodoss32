CREATE DATABASE market_express_simple CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE market_express_simple;

CREATE TABLE usuario(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tipo INT,
    nombre VARCHAR(100),
    apellido VARCHAR(100),
    correo VARCHAR(100),
    contrasena VARCHAR(80) ,
    avatar VARCHAR(80),
    telefono VARCHAR(80),
    direccion VARCHAR(80),
    compras  VARCHAR(80),
    carrito VARCHAR(80)
);

CREATE TABLE roles(
    idRole INT NOT NULL AUTO_INCREMENT PRIMARY kEY,
    nombreRole Varchar(50)
);

INSERT INTO roles (nombreRole) VALUES('SuperAdministrador'),('Administrador'),('Usuario');
ALTER TABLE usuario ADD CONSTRAINT fk_usuario FOREIGN KEY (tipo) REFERENCES roles (idRole);

CREATE TABLE unidad (
    id_unidad INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    unidad VARCHAR(100)
);

INSERT INTO unidad (unidad) VALUES ('Kilo'),('Libra'),('Unidad'),('Docena'),('Cubeta'),('Caja'),('Botella'),('Arroba'),('Bulto'),('Bolsa');

CREATE TABLE Producto(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria INT NOT NULL,
    sub_categoria INT NOT NULL,
    nombre VARCHAR(100),
    precio INT NOT NULL,
    unidad INT NOT NULL,
    caracteristicas VARCHAR(255),
    cantidad INT NOT NULL,
    descuento BOOLEAN,
    valor_descuento NUMERIC
);

CREATE TABLE categorias(
    idCategoria INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(80)
);

INSERT INTO categorias(nombre) VALUES ('Verduras'),('Carnes'),('Viveres'),('Hogar'),('Electronicos'),
                                      ('Electrodomesticos'), ('VideoJuegos'),('Licores'),('Cervezas'),
                                      ('Vinos'), ('AseoPersonal'),('AseoHogar');

CREATE TABLE sub_categorias(
    id_subcategoria INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100)
);  

INSERT INTO sub_categorias(nombre) VALUES  ('Cocina'), ('Decoración') ,('Muebles'),('Dormitorio'),
                                          ('Enlatados'),('Pastas'),('Cereales'),('Aceites'),
                                          ('celulares'),('computadores'),('tablets'),('Televisores'),
                                          ('Neveras'),('Estufas'),('Horno'),('Lavadora'),
                                          ('Accion'),('Arcade'),('Deportivo'),('Estrategia'),
                                          ('Vodka'),('Whisky'),('Ron'),('Aguardiente'),
                                          ('Heineken'),('Corona'),('Budweiser'),('Aguila'),
                                          ('Blanco'), ('Rosado'), ('Tinto'), ('Clarete'),
                                          ('Cerdo'), ('pescado'),('Pollo'),('Res'),
                                          ('jabon'), ('Bucal'),('Desodorante'), ('otros'),
                                          ('Detergentes'),('Suavizante'),('Ambientadores'),('Limpiadores'); 

ALTER TABLE Producto ADD CONSTRAINT categoria_fk FOREIGN KEY (categoria) REFERENCES categorias (idCategoria);
ALTER TABLE Producto ADD CONSTRAINT sub_categoria_fk FOREIGN KEY (sub_categoria)  REFERENCES sub_categorias (id_subcategoria);   
ALTER TABLE Producto ADD CONSTRAINT unidad_fk FOREIGN KEY (unidad) REFERENCES unidad (id_unidad);                                    